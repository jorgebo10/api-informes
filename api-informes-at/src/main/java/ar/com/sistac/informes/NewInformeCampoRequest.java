package ar.com.sistac.informes;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.google.auto.value.AutoValue;

import javax.validation.constraints.NotNull;

@AutoValue
@JsonDeserialize(builder = AutoValue_NewInformeCampoRequest.Builder.class)
public abstract class NewInformeCampoRequest {
    @NotNull
    @JsonProperty("id")
    public abstract long id();

    public static Builder builder() {
        return new AutoValue_NewInformeCampoRequest.Builder();
    }

    @AutoValue.Builder
    public static abstract class Builder {

        @NotNull
        @JsonProperty("id")
        public abstract Builder id(@NotNull long id);

        public abstract NewInformeCampoRequest build();
    }
}
