Feature: Testing a REST API
  Users should be able to submit POST requests to a web service

  Scenario: Informe campo upload
    Given There is no informes created
    When user upload informe with id 2
    Then created status is returned
    And informe with id 2 is created
