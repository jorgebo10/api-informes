package ar.com.sistac;

import cucumber.api.CucumberOptions;
import cucumber.api.SnippetType;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(features = "classpath:Features", format = {"pretty"}, snippets = SnippetType.CAMELCASE)
public class CucumberTest {

}
