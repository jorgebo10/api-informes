package ar.com.sistac;

import ar.com.sistac.configuration.ApiInformesConfiguration;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.web.client.RestTemplate;

@ContextConfiguration
@SpringBootTest
public class BaseIntegration {
    private final ApiInformesConfiguration apiInformesConfiguration;
    private final RestTemplate restTemplate;

    @Autowired
    public BaseIntegration(ApiInformesConfiguration apiInformesConfiguration, RestTemplate restTemplate) {
        this.apiInformesConfiguration = apiInformesConfiguration;
        this.restTemplate = restTemplate;
    }

    public RestTemplate getRestTemplate() {
        return restTemplate;
    }

    protected String getApiInformesLocation() {
        return apiInformesConfiguration.getLocation();
    }
}