package ar.com.sistac.informes;

import org.junit.Assert;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import ar.com.sistac.BaseIntegration;
import ar.com.sistac.configuration.ApiInformesConfiguration;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class InformesDefinitionSteps extends BaseIntegration {

    private static final Logger logger = LoggerFactory.getLogger(InformesDefinitionSteps.class);

    private ResponseEntity<String> responseEntity;

    public InformesDefinitionSteps(ApiInformesConfiguration apiInformesConfiguration, RestTemplate restTemplate) {
        super(apiInformesConfiguration, restTemplate);
    }

    @Given("^There is no informes created$")
    public void thereIsNoInformesCreated() throws Throwable {
    }

    @When("^user upload informe with id (\\d+)$")
    public void userUploadInformeWithId(long id) throws Throwable {
        NewInformeCampoRequest newInformeCampoRequest = NewInformeCampoRequest.builder()
                .id(id)
                .build();

        logger.info(getApiInformesLocation());

        responseEntity = getRestTemplate()
                .postForEntity(getApiInformesLocation(), newInformeCampoRequest, String.class);
    }

    @Then("^created status is returned$")
    public void createdStatusIsReturned() throws Throwable {
        Assert.assertEquals(HttpStatus.CREATED, responseEntity.getStatusCode());
    }

    @And("^informe with id (\\d+) is created$")
    public void informeWithIdIsCreated(int id) throws Throwable {
        Assert.assertEquals(getApiInformesLocation() + "/" + id, responseEntity.getHeaders().get("Location").get(0));
    }
}
