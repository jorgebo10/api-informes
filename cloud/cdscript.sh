 #!/usr/bin/env bash

if [[ "$(uname -s )" == "Linux" ]]; then
  export VIRTUALBOX_SHARE_FOLDER="$PWD:$PWD"
fi

for i in 1 2 3; do
    docker-machine create \
        -d virtualbox \
        --virtualbox-memory 512 \
        swarm-$i
done

eval $(docker-machine env swarm-1)

docker swarm init \
    --advertise-addr $(docker-machine ip swarm-1)

TOKEN=$(docker swarm join-token -q manager)


for i in 2 3; do
    eval $(docker-machine env swarm-$i)

    docker swarm join \
        --token $TOKEN \
        --advertise-addr $(docker-machine ip swarm-$i) \
        $(docker-machine ip swarm-1):2377
done

docker node update --label-add swarm-1
docker node update --label-add swarm-2
docker node update --label-add swarm-3

eval $(docker-machine env swarm-1)

docker network create --driver overlay proxy
docker network create --driver overlay services

export DOCKER_IP=$(docker-machine ip swarm-1)
export CONSUL_SERVER_IP=$(docker-machine ip swarm-1)
export CONSUL_IP=$(docker-machine ip swarm-1)

docker service create --name registry \
    -p 5000:5000 \
    --reserve-memory 100m \
    --mount "type=bind,source=$PWD,target=/var/lib/registry" \
    registry:2.5.0

echo "Registry started"

curl -o docker-compose-proxy.yml https://raw.githubusercontent.com/vfarcic/docker-flow-proxy/master/docker-compose.yml

docker-compose -f docker-compose-proxy.yml \
    up -d consul-server

for i in 2 3; do
    eval $(docker-machine env swarm-$i)

    export DOCKER_IP=$(docker-machine ip swarm-$i)

    docker-compose -f docker-compose-proxy.yml \
        up -d consul-agent
done

rm docker-compose-proxy.yml

echo "Consul started"

docker service create --name proxy \
    -p 80:80 \
    -p 443:443 \
    -p 8090:8080 \
    --network proxy \
    -e MODE=swarm \
    --replicas 3 \
    -e CONSUL_ADDRESS="$(docker-machine ip swarm-1):8500,$(docker-machine ip swarm-2):8500,$(docker-machine ip swarm-3):8500" \
    --reserve-memory 50m \
    vfarcic/docker-flow-proxy

while true; do
    REPLICAS=$(docker service ls | grep proxy | awk '{print $3}')
    REPLICAS_NEW=$(docker service ls | grep proxy | awk '{print $4}')
    if [[ $REPLICAS == "3/3" || $REPLICAS_NEW == "3/3" ]]; then
        break
    else
        echo "Waiting for the proxy service..."
        sleep 10
    fi
done

docker service create --name api-informes-db \
    --network services \
    --reserve-memory 150m \
    mongo:3.2.10

while true; do
    REPLICAS=$(docker service ls | grep api-informes-db | awk '{print $3}')
    REPLICAS_NEW=$(docker service ls | grep api-informes-db | awk '{print $4}')
    if [[ $REPLICAS == "1/1" || $REPLICAS_NEW == "1/1" ]]; then
        break
    else
        echo "Waiting for the api-informes-db service..."
        sleep 10
    fi
done

docker service create --name api-informes \
    -e DATABASE_URL=api-informes-db \
    -e DATABASE_PORT=27017 \
    -e DATABASE_NAME=sistac \
    -e APP_PORT=8080 \
    --network services \
    --network proxy \
    --replicas 2 \
    --update-delay 5s \
    jorgebo10/api-informes:1.3

while true; do
    REPLICAS=$(docker service ls | grep api-informes | awk '{print $3}')
    REPLICAS_NEW=$(docker service ls | grep api-informes | awk '{print $4}')
    if [[ $REPLICAS == "2/2" || $REPLICAS_NEW == "2/2" ]]; then
        break
    else
        echo "Waiting for the api-informes service..."
        sleep 10
    fi
done

curl "$(docker-machine ip swarm-1):8090/v1/docker-flow-proxy/reconfigure?serviceName=api-informes&servicePath=/&port=8080&distribute=true"


echo "Production cluster already configured"

# CD cluster conf

for i in 1 2 3; do
    docker-machine create \
        -d virtualbox \
        --virtualbox-memory 512 \
        swarm-test-$i
done

eval $(docker-machine env swarm-test-1)

docker swarm init \
    --advertise-addr $(docker-machine ip swarm-test-1)

TOKEN=$(docker swarm join-token -q manager)

for i in 2 3; do
    eval $(docker-machine env swarm-test-$i)

    docker swarm join \
        --token $TOKEN \
        --advertise-addr $(docker-machine ip swarm-test-$i) \
        $(docker-machine ip swarm-test-1):2377
done

docker node update --label-add env=jenkins-agent swarm-test-1
docker node update --label-add env=prod-like swarm-test-2
docker node update --label-add env=prod-like swarm-test-3

eval $(docker-machine env swarm-test-1)

docker network create --driver overlay proxy
docker network create --driver overlay services

docker service create --name registry \
    -p 5000:5000 \
    --reserve-memory 100m \
    --mount "type=bind,source=$PWD,target=/var/lib/registry" \
    registry:2.5.0


curl -o docker-compose-proxy.yml https://raw.githubusercontent.com/vfarcic/docker-flow-proxy/master/docker-compose.yml

export DOCKER_IP=$(docker-machine ip swarm-test-1)
export CONSUL_SERVER_IP=$(docker-machine ip swarm-test-1)
export CONSUL_IP=$(docker-machine ip swarm-test-1)

docker-compose -f docker-compose-proxy.yml up -d consul-server

for i in 2 3; do
    eval $(docker-machine env swarm-test-$i)

    export DOCKER_IP=$(docker-machine ip swarm-test-$i)

    docker-compose -f docker-compose-proxy.yml \
        up -d consul-agent
done

rm docker-compose-proxy.yml

docker service create --name proxy \
    -p 80:80 \
    -p 443:443 \
    -p 8090:8080 \
    --network proxy \
    -e MODE=swarm \
    --replicas 2 \
    -e CONSUL_ADDRESS="$(docker-machine ip swarm-test-1):8500,$(docker-machine ip swarm-test-2):8500,$(docker-machine ip swarm-test-3):8500" \
    --constraint 'node.labels.env == prod-like' \
    vfarcic/docker-flow-proxy

while true; do
    REPLICAS=$(docker service ls | grep proxy | awk '{print $3}')
    REPLICAS_NEW=$(docker service ls | grep proxy | awk '{print $4}')
    if [[ $REPLICAS == "2/2" || $REPLICAS_NEW == "2/2" ]]; then
        break
    else
        echo "Waiting for the proxy service..."
        sleep 10
    fi
done

echo ">> The swarm test cluster is up and running"

echo "Configuring jenkins"
echo "Mounting jenkins_home in $PWD/jenkins_home"

eval $(docker-machine env swarm-1)

echo "admin" | docker secret create jenkins-user -
echo "admin" | docker secret create jenkins-pass -

export JENKINS_PORT=8082

docker service create --name jenkins \
    -p ${JENKINS_PORT}:8080 \
    -p 50000:50000 \
    -e JENKINS_OPTS="--prefix=/jenkins" \
    --mount "type=bind,source=$PWD/jenkins_home,target=/var/jenkins_home" --reserve-memory 300m \
    jorgebo10/jenkins:latest

while true; do
    REPLICAS=$(docker service ls | grep jenkins | awk '{print $3}')
    REPLICAS_NEW=$(docker service ls | grep jenkins | awk '{print $4}')
    if [[ $REPLICAS == "1/1" || $REPLICAS_NEW == "1/1" ]]; then
        break
    else
        echo "Waiting for the jenkins service..."
        sleep 10
    fi
done

echo "Configuring sonarqube"

docker service create --name sonarqube \
    -p 9000:9000 \
    -p 9092:9092 \
    sonarqube

while true; do
    REPLICAS=$(docker service ls | grep sonarqube | awk '{print $3}')
    REPLICAS_NEW=$(docker service ls | grep sonarqube | awk '{print $4}')
    if [[ $REPLICAS == "1/1" || $REPLICAS_NEW == "1/1" ]]; then
        break
    else
        echo "Waiting for the sonarqube..."
        sleep 10
    fi
done

echo "Configuring jenkins-agent"

eval $(docker-machine env swarm-test-1)

echo "admin" | docker secret create jenkins-user -
echo "admin" | docker secret create jenkins-pass -

docker-machine ssh swarm-test-1 "sudo mkdir /workspace;sudo chmod 777 /workspace;exit;"

export JENKINS_IP=$(docker-machine ip swarm-1)

docker service create --name jenkins-agent \
    -e COMMAND_OPTIONS="-master http://$(docker-machine ip swarm-1):8082/jenkins -username admin -password admin -labels 'docker' -executors 5" \
    --mode global \
    --constraint 'node.labels.env == jenkins-agent' \
    --mount "type=bind,source=/var/run/docker.sock,target=/var/run/docker.sock" \
    --mount "type=bind,source=$HOME/.docker/machine/machines,target=/machines" \
    --mount "type=bind,source=/workspace,target=/workspace" \
    jorgebo10/jenkins-swarm-agent

while true; do
    REPLICAS=$(docker service ls | grep jenkins-agent | awk '{print $3}')
    REPLICAS_NEW=$(docker service ls | grep jenkins-agent | awk '{print $4}')
    if [[ $REPLICAS == "1/1" || $REPLICAS_NEW == "1/1" ]]; then
        break
    else
        echo "Waiting for the jenkins-agent service..."
        sleep 10
    fi
done

echo "Agents configured"

docker service create --name api-informes-db \
    --network services \
    --reserve-memory 150m \
    --constraint 'node.labels.env == prod-like' \
    mongo:3.2.10

while true; do
    REPLICAS=$(docker service ls | grep api-informes-db | awk '{print $3}')
    REPLICAS_NEW=$(docker service ls | grep api-informes-db | awk '{print $4}')
    if [[ $REPLICAS == "1/1" || $REPLICAS_NEW == "1/1" ]]; then
        break
    else
        echo "Waiting for the api-informes-db service..."
        sleep 10
    fi
done

docker service create --name api-informes \
    -e DATABASE_URL=api-informes-db \
    -e DATABASE_PORT=27017 \
    -e DATABASE_NAME=sistac \
    -e APP_PORT=8080 \
    --network services \
    --network proxy \
    --replicas 2 \
    --constraint 'node.labels.env == prod-like' \
    --update-delay 5s \
    jorgebo10/api-informes:1.3

curl "$(docker-machine ip swarm-test-1):8090/v1/docker-flow-proxy/reconfigure?serviceName=api-informes&servicePath=/&port=8080&distribute=true"


echo ""
echo ">> The services are up and running inside the swarm test cluster"

