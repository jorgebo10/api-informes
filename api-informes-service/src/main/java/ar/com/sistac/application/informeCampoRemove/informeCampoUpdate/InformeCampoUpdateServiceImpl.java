package ar.com.sistac.application.informeCampoRemove.informeCampoUpdate;

import ar.com.sistac.domain.informe.InformeCampoId;
import ar.com.sistac.domain.informe.InformeCampoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service class InformeCampoRemoveServiceImpl implements InformeCampoRemoveUsecase {
    private InformeCampoRepository informeCampoRepository;

    @Autowired
    public InformeCampoRemoveServiceImpl(InformeCampoRepository informeCampoRepository) {
        this.informeCampoRepository = informeCampoRepository;
    }

    @Override
    public void remove(final long id) {
        informeCampoRepository.removeByInformeCampoId(InformeCampoId.create(id));
    }
}
