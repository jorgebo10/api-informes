package ar.com.sistac.application.informeCampoUpdate;

import ar.com.sistac.domain.informe.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class InformeCampoUpdateServiceImpl implements InformeCampoUpdateService {
    private InformeCampoRepository informeCampoRepository;

    @Autowired
    public InformeCampoUpdateServiceImpl(InformeCampoRepository informeCampoRepository) {
        this.informeCampoRepository = informeCampoRepository;
    }

    @Override
    public InformeCampo update(final InformeCampoId informeCampoId, final FacilityAddress facilityAddress,
            final TankIdentification tankIdentification, final TankGeometry tankGeometry) {
        InformeCampo informeCampo = informeCampoRepository.findByInformeCampoId(informeCampoId);
        informeCampo.setFacilityAddress(facilityAddress);
        informeCampo.setTankIdentification(tankIdentification);
        informeCampo.setTankGeometry(tankGeometry);

        return informeCampo;
    }
}
