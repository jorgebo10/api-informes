package ar.com.sistac.application.informeCampoUpdate;

import ar.com.sistac.domain.informe.*;

public interface InformeCampoUpdateService {

    InformeCampo update(final InformeCampoId informeCampoId, final FacilityAddress facilityAddress,
            final TankIdentification tankIdentification, final TankGeometry tankGeometry);
}
