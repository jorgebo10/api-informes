package ar.com.sistac.application.informeCampoCreate;

import ar.com.sistac.domain.informe.InformeCampo;

public interface InformeCampoCreateUsecase {

    InformeCampo createById(final long id);
}
