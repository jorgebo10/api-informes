package ar.com.sistac.application.informeCampoRemove.informeCampoUpdate;

public interface InformeCampoRemoveUsecase {

    void remove(final long id);
}
