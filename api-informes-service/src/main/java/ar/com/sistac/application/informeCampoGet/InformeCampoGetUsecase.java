package ar.com.sistac.application.informeCampoGet;

import ar.com.sistac.domain.informe.InformeCampo;

import java.util.Optional;

public interface InformeCampoGetUsecase {

    Optional<InformeCampo> getByInformeCampoId(final long id);
}
