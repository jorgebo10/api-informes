package ar.com.sistac.application.informeCampoCreate;

import ar.com.sistac.domain.informe.InformeCampo;
import ar.com.sistac.domain.informe.InformeCampoId;
import ar.com.sistac.domain.informe.InformeCampoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.Instant;

@Service
public class InformeCampoCreateUsecaseImpl implements InformeCampoCreateUsecase {

    @Autowired
    private InformeCampoRepository informeCampoRepository;

    public InformeCampoCreateUsecaseImpl(InformeCampoRepository informeCampoRepository) {
        this.informeCampoRepository = informeCampoRepository;
    }

    public InformeCampo createById(final long id) {
        InformeCampo informeCampo = new InformeCampo(InformeCampoId.create(id), Instant.now());
        return informeCampoRepository.save(informeCampo);
    }
}
