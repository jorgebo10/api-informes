package ar.com.sistac.application.informeCampoGet;

import ar.com.sistac.domain.informe.InformeCampo;
import ar.com.sistac.domain.informe.InformeCampoId;
import ar.com.sistac.domain.informe.InformeCampoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class InformeCampoGetUsecaseImpl implements InformeCampoGetUsecase {

    @Autowired
    private InformeCampoRepository informeCampoRepository;

    public InformeCampoGetUsecaseImpl(InformeCampoRepository informeCampoRepository) {
        this.informeCampoRepository = informeCampoRepository;
    }

    @Override
    public Optional<InformeCampo> getByInformeCampoId(long id) {
        return Optional.ofNullable(informeCampoRepository.findByInformeCampoId(InformeCampoId.create(id)));
    }
}
