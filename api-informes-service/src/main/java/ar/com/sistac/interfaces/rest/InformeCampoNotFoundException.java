package ar.com.sistac.interfaces.rest;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NOT_FOUND)
public class InformeCampoNotFoundException extends RuntimeException {

    public InformeCampoNotFoundException(long id) {
        super("could not find informe campo by id '" + id + "'.");
    }
}
