package ar.com.sistac.interfaces.rest;

import org.springframework.hateoas.VndErrors;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

@ControllerAdvice
public class InformeCampoControllerAdvice {

    @ResponseBody
    @ExceptionHandler(InformeCampoNotFoundException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    VndErrors informeCampoNotFoundExceptionHandler(final InformeCampoNotFoundException ex) {
        return new VndErrors("error", ex.getMessage());
    }
}
