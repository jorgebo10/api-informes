package ar.com.sistac.interfaces.rest;

import ar.com.sistac.domain.informe.InformeCampo;
import org.springframework.hateoas.ResourceSupport;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

public class InformeCampoResource extends ResourceSupport {
    private final InformeCampo informeCampo;

    public InformeCampoResource(final InformeCampo informeCampo) {
        this.informeCampo = informeCampo;
        this.add(linkTo(methodOn(InformeCampoController.class, informeCampo.getInformeCampoId().id())
                .get(informeCampo.getInformeCampoId().id()))
                .withSelfRel());
    }

    public InformeCampo getInformeCampo() {
        return informeCampo;
    }
}
