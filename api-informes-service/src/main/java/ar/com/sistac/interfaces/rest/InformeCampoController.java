package ar.com.sistac.interfaces.rest;

import java.net.URI;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.Link;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import ar.com.sistac.application.informeCampoCreate.InformeCampoCreateUsecase;
import ar.com.sistac.application.informeCampoGet.InformeCampoGetUsecase;
import ar.com.sistac.application.informeCampoRemove.informeCampoUpdate.InformeCampoRemoveUsecase;
import ar.com.sistac.domain.informe.InformeCampo;

@RestController
@RequestMapping(value = "/api/informes")
public class InformeCampoController {
    private final InformeCampoCreateUsecase informeCampoCreateUsecase;
    private final InformeCampoGetUsecase informeCampoGetUsecase;
    private final InformeCampoRemoveUsecase informeCampoRemoveUsecase;

    @Autowired
    public InformeCampoController(final InformeCampoCreateUsecase informeCampoCreateUsecase,
            final InformeCampoGetUsecase informeCampoGetUsecase,
            final InformeCampoRemoveUsecase informeCampoRemoveUsecase) {
        this.informeCampoCreateUsecase = informeCampoCreateUsecase;
        this.informeCampoGetUsecase = informeCampoGetUsecase;
        this.informeCampoRemoveUsecase = informeCampoRemoveUsecase;
    }

    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity<?> create(@RequestBody InformeCampoCreateRequest informeCampoCreateRequest) {
        final InformeCampo informeCampo = informeCampoCreateUsecase.createById(informeCampoCreateRequest.id());

        final Link forOneInformeCampo = new InformeCampoResource(informeCampo).getLink("self");

        return ResponseEntity.created(URI.create(forOneInformeCampo.getHref())).build();
    }

    @RequestMapping(method = RequestMethod.GET)
    public void list() {
        System.out.println("MACRI GATO");
    }

    @RequestMapping(method = RequestMethod.GET, value = "/{id}")
    public InformeCampoResource get(@PathVariable long id) {
        return informeCampoGetUsecase.getByInformeCampoId(id)
                .map(this::toResource)
                .orElseThrow(() -> new InformeCampoNotFoundException(id));
    }

    private InformeCampoResource toResource(final InformeCampo informeCampo) {
        return new InformeCampoResource(informeCampo);
    }

    @RequestMapping(method = RequestMethod.DELETE, value = "/{id}")
    public void delete(@PathVariable long id) {
        informeCampoRemoveUsecase.remove(id);
    }
}
