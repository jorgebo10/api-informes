package ar.com.sistac.interfaces.rest;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.google.auto.value.AutoValue;

import javax.validation.constraints.NotNull;

@AutoValue
@JsonDeserialize(builder = AutoValue_InformeCampoCreateRequest.Builder.class)
public abstract class InformeCampoCreateRequest {
    @NotNull
    @JsonProperty("id")
    public abstract long id();

    @NotNull
    public static Builder builder() {
        return new AutoValue_InformeCampoCreateRequest.Builder();
    }

    @AutoValue.Builder
    public static abstract class Builder {

        @NotNull
        @JsonProperty("id")
        public abstract Builder id(@NotNull long id);

        @NotNull
        public abstract InformeCampoCreateRequest build();
    }
}
