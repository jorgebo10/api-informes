package ar.com.sistac.domain.informe;

import org.springframework.data.mongodb.repository.MongoRepository;

import java.time.LocalDateTime;

public interface InformeCampoRepository extends MongoRepository<InformeCampo, String> {
    InformeCampo findByCreatedAt(final LocalDateTime createdAt);

    InformeCampo findByInformeCampoId(final InformeCampoId informeCampoId);

    void removeByInformeCampoId(final InformeCampoId informeCampoId);
}
