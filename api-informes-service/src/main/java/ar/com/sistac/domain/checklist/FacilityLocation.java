package ar.com.sistac.domain.checklist;

import com.google.auto.value.AutoValue;

@AutoValue
public abstract class FacilityLocation {

    public abstract Double latitud();

    public abstract Double longitud();
}
