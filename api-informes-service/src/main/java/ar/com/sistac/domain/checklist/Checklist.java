package ar.com.sistac.domain.checklist;

import ar.com.sistac.domain.informe.InformeCampoId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.time.LocalDateTime;

@Document(collection = "checklists")
public class Checklist {
    @Id
    private String id;
    private LocalDateTime createdAt;
    private InformeCampoId informeCampoId;
}
