package ar.com.sistac.domain.informe;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.google.auto.value.AutoValue;

@AutoValue
@JsonDeserialize
public abstract class TankIdentification {

    public abstract Long cit();

    public abstract String id();

    public abstract int fabricadoAt();

    public static TankIdentification create(Long cit, String id, int fabricadoAt) {
        return new AutoValue_TankIdentification(cit, id, fabricadoAt);
    }
}
