package ar.com.sistac.domain.informe;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.google.auto.value.AutoValue;

@AutoValue
@JsonDeserialize
public abstract class InformeCampoId {
    @JsonProperty("id")
    public abstract long id();

    public static AutoValue_InformeCampoId create(final long id) {
        return new AutoValue_InformeCampoId(id);
    }
}
