package ar.com.sistac.domain.envolvente;

import ar.com.sistac.domain.informe.InformeCampoId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.time.LocalDateTime;

@Document(collection = "envolventeMeasureSet")
public class EnvolventeMeasureSet {
    @Id
    private String id;
    private LocalDateTime createdAt;
    private InformeCampoId informeCampoId;
}
