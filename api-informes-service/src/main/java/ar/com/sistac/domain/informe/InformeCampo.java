package ar.com.sistac.domain.informe;

import ar.com.sistac.domain.checklist.FacilityLocation;
import com.fasterxml.jackson.annotation.JsonIgnore;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.time.Instant;
import java.util.Objects;

@Document(collection = "informesCampo")
public class InformeCampo {
    @Id
    @JsonIgnore
    private String id;
    private Instant createdAt;
    private InformeCampoId informeCampoId;
    private FacilityLocation facilityLocation;
    private FacilityAddress facilityAddress;
    private TankIdentification tankIdentification;
    private TankGeometry tankGeometry;

    public InformeCampo(final InformeCampoId informeCampoId, final Instant createdAt) {
        this.informeCampoId = informeCampoId;
        this.createdAt = createdAt;
    }

    public InformeCampoId getInformeCampoId() {
        return informeCampoId;
    }

    public void setInformeCampoId(InformeCampoId informeCampoId) {
        this.informeCampoId = informeCampoId;
    }

    public FacilityLocation getFacilityLocation() {
        return facilityLocation;
    }

    public void setFacilityLocation(FacilityLocation facilityLocation) {
        this.facilityLocation = facilityLocation;
    }

    public FacilityAddress getFacilityAddress() {
        return facilityAddress;
    }

    public void setFacilityAddress(FacilityAddress facilityAddress) {
        this.facilityAddress = facilityAddress;
    }

    public TankIdentification getTankIdentification() {
        return tankIdentification;
    }

    public void setTankIdentification(TankIdentification tankIdentification) {
        this.tankIdentification = tankIdentification;
    }

    public TankGeometry getTankGeometry() {
        return tankGeometry;
    }

    public void setTankGeometry(TankGeometry tankGeometry) {
        this.tankGeometry = tankGeometry;
    }

    public Instant getCreatedAt() {
        return createdAt;
    }

    public String getId() {
        return id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;
        InformeCampo that = (InformeCampo) o;
        return Objects.equals(informeCampoId, that.informeCampoId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(informeCampoId);
    }
}
