package ar.com.sistac.domain.informe;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.google.auto.value.AutoValue;

@AutoValue
@JsonDeserialize
public abstract class FacilityAddress {

    public abstract String calle();

    public abstract String localidad();

    public abstract String provincia();

    public static FacilityAddress create(String calle, String localidad, String provincia) {
        return new AutoValue_FacilityAddress(calle, localidad, provincia);
    }
}
