package ar.com.sistac;

import ar.com.sistac.domain.informe.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.time.Instant;
import java.time.LocalDate;

@SpringBootApplication
public class Application implements CommandLineRunner {

    @Autowired
    private InformeCampoRepository informeCampoRepository;

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }

    @Override
    public void run(String... strings) throws Exception {
        System.out.println("MACRI GATO");
        informeCampoRepository.deleteAll();
        InformeCampo informeCampo = new InformeCampo(InformeCampoId.create(1), Instant.now());
        informeCampo.setFacilityAddress(FacilityAddress.create("calle", "localidad", "provincia"));
        informeCampo.setTankIdentification(TankIdentification.create(1L, "cit", LocalDate.now().getYear()));

        informeCampoRepository.save(informeCampo);
    }
}
