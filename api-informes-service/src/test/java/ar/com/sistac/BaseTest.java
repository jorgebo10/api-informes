package ar.com.sistac;

import java.time.Instant;

import org.junit.Before;
import org.mockito.Mockito;

import ar.com.sistac.application.informeCampoCreate.InformeCampoCreateUsecase;
import ar.com.sistac.application.informeCampoGet.InformeCampoGetUsecase;
import ar.com.sistac.application.informeCampoRemove.informeCampoUpdate.InformeCampoRemoveUsecase;
import ar.com.sistac.domain.informe.InformeCampo;
import ar.com.sistac.domain.informe.InformeCampoId;
import ar.com.sistac.interfaces.rest.InformeCampoController;
import io.restassured.module.mockmvc.RestAssuredMockMvc;

public class BaseTest {
    protected InformeCampoCreateUsecase informeCampoCreateUsecase;
    protected InformeCampoGetUsecase informeCampoGetUsecase;
    protected InformeCampoRemoveUsecase informeCampoRemoveUsecase;

    @Before
    public void setup() {

        informeCampoCreateUsecase = Mockito.mock(InformeCampoCreateUsecase.class);
        informeCampoGetUsecase = Mockito.mock(InformeCampoGetUsecase.class);
        informeCampoRemoveUsecase = Mockito.mock(InformeCampoRemoveUsecase.class);

        InformeCampo newInformeCampo = new InformeCampo(InformeCampoId.create(1), Instant.now());

        Mockito.when(informeCampoCreateUsecase.createById(1)).thenReturn(newInformeCampo);

        RestAssuredMockMvc.standaloneSetup(new InformeCampoController(informeCampoCreateUsecase, informeCampoGetUsecase, informeCampoRemoveUsecase));
    }
}
