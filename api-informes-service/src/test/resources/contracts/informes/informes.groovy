package contracts.informes

org.springframework.cloud.contract.spec.Contract.make {
    request {
        method 'POST'
        url '/api/informes'
        body([
               "id": "1"
        ])
        headers {
            contentType('application/json')
        }
    }
    response {
        status 201
        headers {
            header('Location', 'http://localhost/api/informes/1')
        }
    }
}